# Changelog

## [0.1.0-a1] - 15.06.2020

### Added

- Modules:
  - `storyfinder.base`: Base classes for algorithms (`Algorithm`) and datasets (`Dataset`)
  - `storyfinder.io`: `VectorDataset` class for handling vector datasets
  - `storyfinder.utils`: `Result` class for handling results of an algorithm
- Subpackage `storyfinder.analyze`
  - Modules:
    - `storyfinder.analyze.hotspot`: `HotSpot` class for performing a hotspot analysis with [G_Local](https://pysal.org/esda/generated/esda.G_Local.html#esda.G_Local) from [PySAL's ESDA package](https://pysal.org/esda/)
    - `storyfinder.analyze.utils`:
      - Functions:
        - `morans_i` for calculating Moran's Index with [Moran](https://pysal.org/esda/generated/esda.Moran.html#esda.Moran) from [PySAL's ESDA package](https://pysal.org/esda/)
        - `spatial_weights` for generating contiguity weights ([Queen](https://pysal.org/libpysal/generated/libpysal.weights.Queen.html#libpysal.weights.Queen), [Rook](https://pysal.org/libpysal/generated/libpysal.weights.Rook.html#libpysal.weights.Rook)) or a [distance band](https://pysal.org/libpysal/generated/libpysal.weights.DistanceBand.html#libpysal.weights.DistanceBand) for usage in PySAL's API.
