FROM continuumio/miniconda3:latest

ADD env-dev.yml /tmp/env.yml

RUN conda update -n base -c defaults conda && \
    conda env update -n base -f /tmp/env.yml
