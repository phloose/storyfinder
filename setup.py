from setuptools import setup, find_packages

with open("README.md") as readme_file:
    long_description = readme_file.read()

with open("CHANGELOG.md") as changelog_file:
    changes = changelog_file.read()

setup(
    name="storyfinder",
    author="Philipp Loose",
    author_email="philipp.loose@hcu-hamburg.de",
    url="https://gitlab.com/phloose/storyfinder",
    description="A python tool for geostatistical analysis and visualization",
    long_description=long_description + "\n\n" + changes,
    keywords="geostatistics visualization geovisual analytics gis geoinformatics",
    license="MIT",
    packages=find_packages(exclude=["tests", "docs"]),
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Intended Audience :: Developers",
        "License :: OSI Approved :: MIT License",
        "Programming Language :: Python :: 3.6",
        "Programming Language :: Python :: 3.7",
        "Programming Language :: Python :: 3.8",
        "Operating System :: Unix",
        "Operating System :: POSIX",
        "Operating System :: Microsoft :: Windows",
    ],
    python_requires=">=3.6",
    extras_require={
        "dev": [
            "pytest>=5.1.0",
            "pytest-cov>=2.7.1",
            "pytest-mock>=1.10.4",
            "flake8>=3.7.8",
            "black>=19.3b0",
        ]
    },
    setup_requires=["setuptools_scm"],
    use_scm_version=True,
    project_urls={
        "Changelog": "https://gitlab.com/phloose/storyfinder/blob/master/CHANGELOG.md",
        "Bug Tracker": "https://gitlab.com/phloose/storyfinder/issues",
    },
)
