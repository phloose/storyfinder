# StoryFinder

A python tool for geostatistical analysis. It also acts as the backend for [StoryFinder-UI](https://gitlab.com/phloose/storyfinder-ui)

## Motivation

Geostatistical analysis can be a daunting task. To get things running often special expert
software is needed which can be overwhelming with a lot of options and statistical
terminology. While not unimportant this barrier holds back especially interested laymen
that want to get further insights into a spatial dataset at hand. The aim of StoryFinder
is to make geostatistical analysis more accessible by using a simple API and a clear
documentation of the options in laymen terms.

## Installation

At first either clone or download this repository and change into it.

```bash
git clone git@gitlab.com:phloose/storyfinder.git
cd storyfinder
```

The recommended way of installing StoryFinder is to use the supplied `env.yml` with the
[conda package manager](https://docs.conda.io/en/latest/) (distributed via [Miniconda](https://docs.conda.io/en/latest/miniconda.html)).
Especially for windows users the conda package manager facilitates resolving binary dependencies, which otherwise can
cause a lot of trouble when doing this manually.

After Miniconda is installed use conda with the supplied `env.yml` like so:

`conda env create -f env.yml`

After this step has finished a new conda enviroment containing all necessary dependencies
has been created with the name `storyfinder`. Prior to installing the `storyfinder`
python package it needs to be activated:

`conda activate storyfinder`

Then install storyfinder via pip:

`pip install .`

## Usage

You can use storyfinder within a python script, Jupyter Notebook or the python shell. There is also a binder repository where you can try out StoryFinder in a Jupyter notebook:

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/phloose%2Fstoryfinder-nb/master?urlpath=lab%2Ftree%2Fnotebooks)

In any of these cases you need to import the `storyfinder.analyze` subpackage. It holds all
geostatistical algorithm implementations.

In the example below a HotSpot analysis is performed. It expects a vector dataset that
is made accessible via the `VectorDataset` object from the `storyfinder.io` module.

Use it like so:

```python
$ python
Python 3.7.6 | packaged by conda-forge | (default, Mar 23 2020, 23:03:20)
[GCC 7.3.0] on linux
Type "help", "copyright", "credits" or "license" for more information.
>>> from storyfinder.analyze import HotSpot
>>> from storyfinder.io import VectorDataset
>>> hs = HotSpot(VectoDataset("path-to-dataset.shp"), "attribute-of-interest")
>>> hs.calc()
```

After calling `hs.calc()` the results of the analysis are accessible via the `hs.result`
property. It exposes three methods for returning the results:

- `hs.result.as_geojson()` - The result as a GeoJSON-like mapping
- `hs.result.to_gdf()` - The result as a geopandas GeoDataFrame
- `hs.result.to_file("path-to-file.geojson", driver="GeoJSON")` - The result exported to file (when omitting `driver` the same format as the input dataset is used for exporting)

For the case of the `HotSpot` algorithm 4 additional attributes are added to the properties of the dataset of interest when using one
of the methods outlined above:

- `Zs_<attribute-of-interest>` - The z-score for the attribute of interest
- `p_norm_<attribute-of-interest>` - The p-value for the attribute of interest
- `z_sim_<attribute-of-interest>` - The z-score based on statiscial inference
- `p_sim_<attribute-of-interest>` - The p-value based on statistical inference

The `Zs_<attribute-of-interest>` is the most meaningful and with a value above **1.65** denotes a hotspot and below **-1.65** denotes a coldspot.

These attributes can also be accessd via `hs.result.Zs` or `hs.result.p_norm` etc.

For greater detail on the these special attributes have a look at the documentation of [PySAL's esda package](https://pysal.org/esda/) for the [G_Local](https://pysal.org/esda/generated/esda.G_Local.html#esda.G_Local) algorithm and the following links:

Getis, A. and Ord, J.K. (1992), The Analysis of Spatial Association by Use of Distance Statistics. Geographical Analysis, 24: 189-206. <a href="https://doi.org/10.1111/j.1538-4632.1992.tb00261.x" target="_blank">doi:10.1111/j.1538-4632.1992.tb00261.x</a>

Ord, J.K. and Getis, A. (1995), Local Spatial Autocorrelation Statistics: Distributional Issues and an Application. Geographical Analysis, 27: 286-306. <a href="https://doi.org/10.1111/j.1538-4632.1995.tb00912.x" target="_blank">doi:10.1111/j.1538-4632.1995.tb00912.x</a>

<a href="https://desktop.arcgis.com/de/arcmap/10.3/tools/spatial-statistics-toolbox/h-how-hot-spot-analysis-getis-ord-gi-spatial-stati.htm" target="_blank">Hot-Spot-Analysis (ArcGIS help)</a>
