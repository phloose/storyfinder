import pytest

from storyfinder.base import Dataset, Algorithm


class DatasetA(Dataset):
    pass


class TestDataset:
    def test_load_not_overridden(self):
        with pytest.raises(NotImplementedError):
            DatasetA("uri")


class AlgorithmA(Algorithm):
    def impl(self):
        return "Result"

    @Algorithm.result.setter
    def result(self, value):
        self._result = value


class AlgorithmB(Algorithm):
    def impl(self):
        return "Result"


class AlgorithmC(Algorithm):
    @Algorithm.result.setter
    def result(self, value):
        self._result = value


class TestAlgorithm:
    def test_result_access_through_property(self):
        a = AlgorithmA()
        a.calc()
        assert a.result == "Result"

    def test_result_access_fails_when_calc_not_called(self, caplog):
        a = AlgorithmA()
        a.result
        assert f"'{a.__class__.__name__}.calc()' needs to be called" in caplog.text

    def test_setter_not_overridden(self):
        b = AlgorithmB()
        with pytest.raises(NotImplementedError):
            b.calc()

    def test_impl_not_overridden(self):
        c = AlgorithmC()
        with pytest.raises(NotImplementedError):
            c.calc()
