import pytest
import numpy as np

from storyfinder.utils import Result


class DummyAlgorithm:
    def __init__(self):
        self.a = [1, 2, 3]
        self.b = [4, 5, 6]
        self.c = [7, 8, 9]


@pytest.fixture
def fake_dataset(mocker):
    mock_dataset = mocker.Mock(name="MockDataset")
    mock_dataset.meta = {"schema": {"properties": {"d": "float"}}, "driver": "GeoJSON"}
    mock_dataset.rows = [
        {"properties": {"d": 10}},
        {"properties": {"d": 11}},
        {"properties": {"d": 12}},
    ]
    return mock_dataset


@pytest.fixture
def fake_dataset_with_geoms(fake_dataset):
    for row in fake_dataset.rows:
        row.update({"geometry": {"type": "Point", "coordinates": [0, 0]}})
    fake_dataset.meta["crs"] = {"init": "epsg:4326"}
    return fake_dataset


class TestResult:
    def test_init_fails_when_wanted_is_not_list_or_tuple(self):
        with pytest.raises(TypeError):
            Result(DummyAlgorithm(), None, None, None)

    def test_init_fails_when_attribute_does_not_exist_in_algorithm_obj(self):
        with pytest.raises(AttributeError):
            Result(DummyAlgorithm(), None, None, ["z"])

    def test_to_file_uses_correct_schema(self, fake_dataset, mocker):
        # Mock fiona since we don't want to write to real a file
        mock_fiona = mocker.MagicMock(name="FionaMock")

        mocker.patch("storyfinder.utils.fiona", mock_fiona)

        r = Result(DummyAlgorithm(), fake_dataset, "attr", ["a", "b", "c"])
        r.to_file("somepath")

        mock_fiona.open.assert_called_with(
            "somepath",
            "w",
            driver="GeoJSON",
            schema={
                "properties": {
                    "a_attr": "float",
                    "b_attr": "float",
                    "c_attr": "float",
                    "d": "float",
                }
            },
        )

    def test_to_file_writes_correct_rows(self, fake_dataset, mocker):
        # Mock fiona since we don't want to write to real a file
        mock_fiona = mocker.MagicMock(name="FionaMock")

        # Since fiona.open is used as a context manager we need to tell the mock what
        # to return when __enter__ is called.
        mock_fiona_out = mocker.Mock(name="FionaOutMock")
        mock_fiona.open.return_value.__enter__.return_value = mock_fiona_out

        mocker.patch("storyfinder.utils.fiona", mock_fiona)

        r = Result(DummyAlgorithm(), fake_dataset, "attr", ["a", "b", "c"])
        r.to_file("somepath")

        mock_fiona_out.writerecords.assert_called_with(
            [
                {"properties": {"a_attr": 1.0, "b_attr": 4.0, "c_attr": 7.0, "d": 10}},
                {"properties": {"a_attr": 2.0, "b_attr": 5.0, "c_attr": 8.0, "d": 11}},
                {"properties": {"a_attr": 3.0, "b_attr": 6.0, "c_attr": 9.0, "d": 12}},
            ]
        )

    def test_to_gdf_shape_called_correctly(self, fake_dataset_with_geoms, mocker):
        mock_geopandas = mocker.Mock(name="GeopandasMock")
        mock_shapely = mocker.Mock(name="ShapelyMock")

        mocker.patch("storyfinder.utils.gpd", mock_geopandas)
        mocker.patch("storyfinder.utils.sg", mock_shapely)

        r = Result(DummyAlgorithm(), fake_dataset_with_geoms, "attr", ["a", "b", "c"])
        r.to_gdf()

        assert mock_shapely.shape.call_count == 3

    def test_to_gdf_GeoDataFrame_called_correctly(
        self, fake_dataset_with_geoms, mocker
    ):
        mock_geopandas = mocker.Mock(name="GeopandasMock")
        mock_shapely = mocker.Mock(name="ShapelyMock")

        # sg.shape should just return what it is called with
        mock_shapely.shape = lambda self: self

        mocker.patch("storyfinder.utils.gpd", mock_geopandas)
        mocker.patch("storyfinder.utils.sg", mock_shapely)

        r = Result(DummyAlgorithm(), fake_dataset_with_geoms, "attr", ["a", "b", "c"])
        r.to_gdf()

        expected_props = (
            {"a_attr": 1.0, "b_attr": 4.0, "c_attr": 7.0, "d": 10},
            {"a_attr": 2.0, "b_attr": 5.0, "c_attr": 8.0, "d": 11},
            {"a_attr": 3.0, "b_attr": 6.0, "c_attr": 9.0, "d": 12},
        )

        # #23 geopandas complains when the geometries are not supplied as a list
        expected_geoms = [
            {"type": "Point", "coordinates": [0, 0]},
            {"type": "Point", "coordinates": [0, 0]},
            {"type": "Point", "coordinates": [0, 0]},
        ]

        mock_geopandas.GeoDataFrame.assert_called_with(
            expected_props, geometry=expected_geoms, crs={"init": "epsg:4326"}
        )

    def test_as_geojson(self, fake_dataset_with_geoms):
        r = Result(DummyAlgorithm(), fake_dataset_with_geoms, "attr", ["a", "b", "c"])
        geojson = r.as_geojson()

        expected = {
            "type": "FeatureCollection",
            "features": [
                {
                    "type": "Feature",
                    "properties": {
                        "a_attr": 1.0,
                        "b_attr": 4.0,
                        "c_attr": 7.0,
                        "d": 10,
                    },
                    "geometry": {"type": "Point", "coordinates": [0, 0]},
                },
                {
                    "type": "Feature",
                    "properties": {
                        "a_attr": 2.0,
                        "b_attr": 5.0,
                        "c_attr": 8.0,
                        "d": 11,
                    },
                    "geometry": {"type": "Point", "coordinates": [0, 0]},
                },
                {
                    "type": "Feature",
                    "properties": {
                        "a_attr": 3.0,
                        "b_attr": 6.0,
                        "c_attr": 9.0,
                        "d": 12,
                    },
                    "geometry": {"type": "Point", "coordinates": [0, 0]},
                },
            ],
        }

        assert geojson == expected

    def test_issue_26_return_0_instead_of_NaN(self, fake_dataset_with_geoms):

        dummy = DummyAlgorithm()
        dummy.e = [np.NaN, np.NaN, 5]  # Add a faulty algorithm result to the dummy

        r = Result(dummy, fake_dataset_with_geoms, "attr", ["a", "e"])

        # As undesired behaviour happend mostly when using the as_geojson method (which
        # then returns an invalid JSON) we use it here to test for correct behaviour.
        # But this also applies to the other export or conversion methods of the
        # result object.

        geojson = r.as_geojson()

        expected = {
            "type": "FeatureCollection",
            "features": [
                {
                    "type": "Feature",
                    "properties": {"a_attr": 1.0, "e_attr": 0, "d": 10},
                    "geometry": {"type": "Point", "coordinates": [0, 0]},
                },
                {
                    "type": "Feature",
                    "properties": {"a_attr": 2.0, "e_attr": 0, "d": 11},
                    "geometry": {"type": "Point", "coordinates": [0, 0]},
                },
                {
                    "type": "Feature",
                    "properties": {"a_attr": 3.0, "e_attr": 5.0, "d": 12},
                    "geometry": {"type": "Point", "coordinates": [0, 0]},
                },
            ],
        }

        assert geojson == expected
