import pytest

from storyfinder.analyze.hotspot import HotSpot


@pytest.fixture(autouse=True)
def mock_esda(mocker):
    mock_esda = mocker.Mock(name="esdaMock")
    mocker.patch("storyfinder.analyze.hotspot.esda", mock_esda)
    return mock_esda


@pytest.fixture(autouse=True)
def mock_spatial_weights(mocker):
    mock_spatial_weights = mocker.Mock(name="SpatialWeightsMock")
    mocker.patch("storyfinder.analyze.hotspot.spatial_weights", mock_spatial_weights)
    return mock_spatial_weights


@pytest.fixture(autouse=True)
def mock_result(mocker):
    mock_result = mocker.Mock(name="ResultMock")
    mocker.patch("storyfinder.analyze.hotspot.Result", mock_result)
    return mock_result


@pytest.fixture
def fake_dataset(mocker):
    mock_dataset = mocker.Mock(name="MockDataset")
    mock_dataset.meta = {"schema": {"properties": {"d": "float"}}, "driver": "GeoJSON"}
    mock_dataset.rows = [
        {"properties": {"d": 10}},
        {"properties": {"d": 11}},
        {"properties": {"d": 12}},
    ]
    mock_dataset.properties = [
        (i, row["properties"]) for i, row in enumerate(mock_dataset.rows)
    ]
    return mock_dataset


@pytest.fixture
def fake_dataset_with_geoms(fake_dataset):
    for row in fake_dataset.rows:
        row.update({"geometry": {"type": "Point", "coordinates": [0, 0]}})
    fake_dataset.meta["crs"] = {"init": "epsg:4326"}
    return fake_dataset


class TestHotSpot:
    def test_init_warns_when_wtype_is_d_and_crs_epsg_4326(
        self, fake_dataset_with_geoms, recwarn
    ):
        HotSpot(fake_dataset_with_geoms, "d", wtype="d", threshold=1000)
        warning = recwarn.pop(UserWarning)
        assert "A dataset with geographic coodinates is being used!" in str(
            warning.message
        )

    def test_impl_default_params(
        self, fake_dataset_with_geoms, mock_spatial_weights, mock_esda
    ):
        hs = HotSpot(fake_dataset_with_geoms, "d")
        hs.impl()

        mock_esda.G_Local.assert_called_with(
            [10, 11, 12], mock_spatial_weights.return_value, star=True, transform="B"
        )
        mock_spatial_weights.assert_called_with(
            fake_dataset_with_geoms, wtype="q", threshold=1000
        )

    def test_impl_user_params(
        self, fake_dataset_with_geoms, mock_spatial_weights, mock_esda
    ):
        hs = HotSpot(fake_dataset_with_geoms, "d", wtype="q", star=False, threshold=1)
        hs.impl()

        mock_esda.G_Local.assert_called_with(
            [10, 11, 12], mock_spatial_weights.return_value, star=False, transform="B"
        )
        mock_spatial_weights.assert_called_with(
            fake_dataset_with_geoms, wtype="q", threshold=1
        )

    def test_calc_calls_impl(self, fake_dataset_with_geoms, mocker):
        mock_impl = mocker.Mock(name="HotSpot.impl()Mock")
        mocker.patch("storyfinder.analyze.hotspot.HotSpot.impl", mock_impl)

        hs = HotSpot(fake_dataset_with_geoms, "d", wtype="q", star=False, threshold=1)
        hs.calc()

        mock_impl.assert_called()

    def test_calc_sets_result(self, fake_dataset_with_geoms, mock_result, mocker):
        mock_impl = mocker.Mock(name="HotSpot.impl()Mock", return_value="impl_result")
        mocker.patch("storyfinder.analyze.hotspot.HotSpot.impl", mock_impl)

        hs = HotSpot(fake_dataset_with_geoms, "d")
        hs.calc()

        mock_result.assert_called_with(
            algoInst="impl_result",
            dataset=fake_dataset_with_geoms,
            dataset_attr="d",
            wanted=["p_sim", "z_sim", "Zs", "p_norm"],
        )
