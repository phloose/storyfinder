import json

import pytest

from storyfinder.io import VectorDataset
from storyfinder.analyze.hotspot import HotSpot


@pytest.fixture(scope="module")
def open_test_data():
    with open("tests/testdata/berlin.geojson") as test_data:
        yield test_data.read()


@pytest.fixture(scope="module")
def reference_test_data():
    with open("tests/testdata/berlin_analyzed.geojson") as reference_test_data:
        yield reference_test_data.read()


@pytest.fixture
def test_data(open_test_data, tmp_path):
    testdir = tmp_path / "test_data"
    testdir.mkdir()
    testfile = testdir / "test_data.geojson"
    testfile.touch()
    testfile.write_text(open_test_data)
    return testfile


def test_hotspot_with_real_data_to_file(test_data, reference_test_data, tmpdir):
    hs = HotSpot(VectorDataset(test_data), "median_pri", wtype="q")
    hs.calc()
    analyzed = tmpdir.mkdir("analyzed").join("test_data_analyzed.geojson")
    hs.result.to_file(analyzed)
    assert analyzed.read() == reference_test_data


def test_hotspot_with_real_data_as_geojson(test_data, reference_test_data):
    hs = HotSpot(VectorDataset(test_data), "median_pri", wtype="q")
    hs.calc()
    assert json.loads(json.dumps(hs.result.as_geojson())) == json.loads(
        reference_test_data
    )
