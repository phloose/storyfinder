"""Base classes for datasets and algorithms"""

import logging

logger = logging.getLogger("storyfinder.base")


class Dataset:
    """Base class of a dataset"""

    def __init__(self, uri):
        self.uri = uri
        self._load()

    def _load(self):
        raise NotImplementedError


class Algorithm:
    """Base class of an algorithm"""

    def calc(self):
        self.result = self.impl()

    def impl(self):
        raise NotImplementedError

    @property
    def result(self):
        try:
            return self._result
        except AttributeError:
            logger.info(
                f"'{self.__class__.__name__}.calc()' needs to be called before "
                "accessing the result"
            )

    @result.setter
    def result(self, value):
        raise NotImplementedError
