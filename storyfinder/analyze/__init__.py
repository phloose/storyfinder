"""storyfinder analyze

The storyfinder.analyze package contains implementations of geostatistical algorithms.
"""
from storyfinder.analyze.hotspot import HotSpot
from storyfinder.analyze.utils import morans_i, spatial_weights
