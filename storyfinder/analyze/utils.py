"""storyfinder utilities"""
from typing import Union

import esda
import numpy as np
import shapely.geometry as sg
from libpysal.weights import DistanceBand, Queen, Rook

from storyfinder.io import VectorDataset


def spatial_weights(
    dataset: VectorDataset, wtype="r", **kwargs
) -> Union[Queen, Rook, DistanceBand]:
    """Calculate a spatial weights matrix for a dataset

    Args:
        dataset (VectorDataset): The input dataset.
        wtype (str, optional): Weight type ("q" = Queen, "r" = Rook). Defaults to "r".
        **kwargs: (dict, optional): Optional keyword arguments to Rook, Queen or
            DistanceBand
    Raises:
        AttributeError: When wtype is not "r", "q" or "d" an AttributeError is raised.

    Returns:
        Union[Queen, Rook, DistanceBand]: Spatial weights object for the given dataset.
    """
    geometries = [sg.shape(geometry) for i, geometry in dataset.geometries]
    threshold = kwargs.pop("threshold", None)
    if wtype == "r":
        wtype = Rook
    elif wtype == "q":
        wtype = Queen
    elif wtype == "d":
        wtype = DistanceBand
        geometries = [np.hstack(geometry.centroid.coords) for geometry in geometries]
        kwargs.update(threshold=threshold)
    else:
        raise AttributeError(
            "'wtype' can only be 'r' (Rook), 'q' (Queen) or 'd' (DistanceBand)!"
        )
    return wtype(geometries, **kwargs)


def morans_i(dataset: VectorDataset, attribute: str, wtype="r") -> esda.Moran:
    """Calculates Moran’s I Global Autocorrelation Statistic for a given dataset and
    attribute

    Args:
        dataset (VectorDataset): The input dataset.
        attribute (str): The attribute to evaluate.
        wtype (str, optional): Weight type ("q" = Queen, "r" = Rook). Defaults to "r".

    Returns:
        esda.Moran: Moran object containing information about spatial autocorrelaion for
            the given dataset and attribute.
    """
    return esda.Moran(
        [prop[attribute] for i, prop in dataset.properties],
        spatial_weights(dataset=dataset, wtype=wtype),
    )
