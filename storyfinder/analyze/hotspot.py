"""storyfinder hotspot analysis"""
import warnings

import esda
import numpy as np

from storyfinder.analyze.utils import spatial_weights
from storyfinder.base import Algorithm
from storyfinder.utils import Result


class HotSpot(Algorithm):
    """Apply pysal.esda.G_Local to a dataset for a given attribute.

    G_Local is based on the Getis-Ord Gi-star algorithm for detecting statistically
    signifcant hot or coldspots in a dataset for a specified attribute. The aim of this
    class is to facilitate the usage of the G_Local algorithm.

    For more information have a look at:

    - https://pysal.org/esda/generated/esda.G_Local.html#esda-g-local
    - http://dx.doi.org/10.1111/j.1538-4632.1992.tb00261.x
    - http://dx.doi.org/10.1111/j.1538-4632.1995.tb00912.x

    Attributes:
        dataset (VectorDataset): The dataset to analyze
        attribute (str): The attribute of interest
        star (boolean): Whether or not to include the attribute value of the actual
            location. Defaults to True.
        wtype (str): The weight for the neighborhood detection. Can be one of 'r'
            (Rook-contiguity), 'q' (Queen-contiguity) or 'd' (distance-based).
            Defaults to 'q'.
        threshold (Union[int, float]): The distance around a features which is
            considered to be its neighborhood. Applies only when using distance-based
            weight type. Defaults to 1000.

    Note:
        When using distance-based weighting the dataset needs to be projected, otherwise
        distance calculations will use degrees and be misleading. A warning is issued
        when the dataset's crs is 'epsg:4326' or not specified.
    """

    def __init__(self, dataset, attribute, star=True, wtype="q", threshold=1000):
        """Initializes HotSpot."""
        self.dataset = dataset
        self.attribute = attribute
        self.star = star
        self.wtype = wtype
        self.threshold = threshold
        if wtype == "d":
            self._check_crs_for_distance_weight(self.dataset)

    def impl(self):
        """Apply esda.G_Local to the specified dataset and attribute.

        Returns:
            esda.G_Local: A pysal.esda.G_Local instance holding geostatistical relevant
                informations from the hot-/coldspot analyis.
        """
        np.random.seed(12345)
        return esda.G_Local(
            [prop[self.attribute] for i, prop in self.dataset.properties],
            spatial_weights(self.dataset, wtype=self.wtype, threshold=self.threshold),
            star=self.star,
            transform="B",
        )

    @Algorithm.result.setter
    def result(self, algoInst):
        """Set result of HotSpot.impl()

        Args:
            algoInst (esda.G_Local): A pysal.esda.G_Local instance holding
                geostatistical relevant informations from the hot-/coldspot analyis
        """
        self._result = Result(
            algoInst=algoInst,
            dataset=self.dataset,
            dataset_attr=self.attribute,
            wanted=["p_sim", "z_sim", "Zs", "p_norm"],
        )

    def _check_crs_for_distance_weight(self, dataset):
        """Issue a warning if crs of a dataset is epsg:4326 or not specified.

        Only used when wtype='d'

        Args:
            dataset (VectorDataset): A dataset instance.
        """
        if "epsg:4326" in dataset.meta["crs"].values():
            warnings.warn(
                "A dataset with geographic coodinates is being used!"
                " Distance calculations will be misleading!"
            )
