"""storyfinder logging settings"""
import logging


class LevelFilter(logging.Filter):
    def __init__(self, exclude):
        self.exclude = exclude

    def filter(self, record):
        if record.levelno in self.exclude:
            return False
        return True


userinfo = logging.StreamHandler()
verbose = logging.StreamHandler()

userinfo.setLevel(logging.INFO)
verbose.setLevel(logging.DEBUG)

simple_formatter = logging.Formatter("%(message)s")
verbose_formatter = logging.Formatter(
    "%(name)s - %(levelname)s - %(message)s - Traceback: %(exc_info)s"
)

userinfo.setFormatter(simple_formatter)
userinfo.addFilter(
    LevelFilter([logging.DEBUG, logging.WARNING, logging.ERROR, logging.CRITICAL])
)

verbose.setFormatter(verbose_formatter)
verbose.addFilter(LevelFilter([logging.INFO]))
