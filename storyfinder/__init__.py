"""storyfinder"""
import logging

from pkg_resources import get_distribution, DistributionNotFound

from storyfinder.log import userinfo, verbose

try:
    __version__ = get_distribution("storyfinder").version
except DistributionNotFound:
    __version__ = "Please install package via setup.py"


logger = logging.getLogger("storyfinder")
logger.setLevel(logging.INFO)
logger.addHandler(userinfo)
logger.addHandler(verbose)
