"""Utilities"""

import copy
import logging

import fiona
import numpy as np
import shapely.geometry as sg
import geopandas as gpd

logger = logging.getLogger("storyfinder.utils")


class Result:
    """Provides methods for accessing and saving results of a geostatistical algorithm.

    Attributes:
        algoInst: An algorithm instance or instance object with attributes of interest,
            which are either a 1-D numpy array or a list/tuple.
        dataset (VectorDataset): The original dataset.
        dataset_attr (str): The attribute of the original dataset used in the algorithm.
        wanted (list/tuple): Attributes of interest that should be made accessible.

    Raises:
        TypeError: Raised when wanted is not a list or tuple.
    """

    def __init__(self, algoInst, dataset, dataset_attr, wanted):
        """Initializes Result"""
        self.algoInst = algoInst
        self.dataset = dataset
        self.dataset_attr = dataset_attr
        if not isinstance(wanted, (tuple, list)):
            raise TypeError(
                "'wanted' needs to be a list or tuple of wanted result attributes! "
                f"Got: {type(wanted)}"
            )
        self.wanted = wanted
        self._new_props = self._get_properties()
        self._new_rows = self._new_props_to_rows()

    def to_file(self, path, driver=None):
        """Write the results of a calculation for a dataset to a file

        Uses the fiona API for writing to file.

        Args:
            path (str): The path to write to.
            driver (str, optional): The GDAL driver (can be one of: ESRI Shapefile,
                GeoJSON, GeoPackage). If None the initial driver of the dataset will be
                used.
        """

        # Make a copy as we don't want to mutate the original dataset
        # TODO: Check performance with big datasets
        schema = copy.deepcopy(self.dataset.meta["schema"])
        driver = driver if driver else self.dataset.meta["driver"]

        # As most calculations return floating point numbers we default to float for the
        # datatype
        schema["properties"].update(
            {f"{prop}_{self.dataset_attr}": "float" for prop in self._new_props.keys()}
        )

        with fiona.open(path, "w", schema=schema, driver=driver) as out:
            out.writerecords(self._new_rows)

    def to_gdf(self):
        """Export the results of a calculation for a dataset to a GeoDataFrame

        Returns:
            GeoDataFrame: A GeoDataFrame with the newly calculated values
        """
        props, geoms = zip(
            *[
                (feature["properties"], sg.shape(feature["geometry"]))
                for feature in self._new_rows
            ]
        )
        # #23 geopandas complains when the geometries are not supplied as a list
        return gpd.GeoDataFrame(
            props, geometry=list(geoms), crs=self.dataset.meta["crs"]
        )

    def as_geojson(self):
        """Export the results of a calculation for a dataset as a GeoJSON conform
        dictionary

        Returns:
            dict: A dictionary with GeoJSON structure and the newly calculated values
        """
        # TODO: Research for the crs parameter (RFC7946)
        return {
            "type": "FeatureCollection",
            "features": [
                {
                    "type": "Feature",
                    "properties": feature["properties"],
                    "geometry": feature["geometry"],
                }
                for feature in self._new_rows
            ],
        }

    def _get_properties(self):
        """Extract properties of interest from the algorithm instance

        Returns:
            dict: A mapping in the form 'attribute_name': [attribute_values,]
        """
        return {attr: getattr(self.algoInst, attr) for attr in self.wanted}

    def _new_props_to_rows(self):
        """Inserts the results of the calculation into the properties dictionary of the
        initial dataset

        To not mutate the original dataset a copy is made.

        Returns:
            rows (list): The rows of the original dataset with additional properties
        """

        # Make a copy as we don't want to mutate the original dataset
        # TODO: Check performance with big datasets
        rows = copy.deepcopy(self.dataset.rows)

        # For every new property that should be appended to the newly created dataset
        # get the value of the _new_props dictionary (which is a list or numpy array in
        # the same order as the features) and insert it into the features properties
        # dict.
        # TODO: Check performance with big datasets
        for prop in self._new_props.keys():
            for feature, prop_val in zip(rows, self._new_props[prop]):
                feature["properties"][f"{prop}_{self.dataset_attr}"] = (
                    float(prop_val) if not np.isnan(prop_val) else 0
                )

        return rows

    def __getattr__(self, attr):
        try:
            return getattr(self.algoInst, attr)
        except AttributeError:
            raise AttributeError(
                f"Neither '{self.__class__.__name__}' nor"
                f" '{type(self.algoInst).__name__}' has attribute '{attr}'"
            ) from None

    def __repr__(self):
        return (
            f"<Result of algorithm '{type(self.algoInst).__name__}' for dataset"
            f" '{self.dataset.uri}' and attribute '{self.dataset_attr}'>"
        )
