"""storyfinder io"""
import fiona

from storyfinder.base import Dataset


class VectorDataset(Dataset):
    """Representation of a vector dataset"""

    def _load(self):
        with fiona.open(self.uri) as data:
            self._rows = list(data)
            self.meta = data.meta

    @property
    def rows(self):
        return self._rows

    @property
    def geometries(self):
        for i, row in enumerate(self.rows):
            yield i, row["geometry"]

    @property
    def properties(self):
        for i, row in enumerate(self.rows):
            yield i, row["properties"]


class RasterDataset(Dataset):
    """Representation of a raster dataset"""

    pass
